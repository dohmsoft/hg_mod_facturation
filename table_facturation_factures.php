<?php
use dsTools\core\tableProxyV2;
use dsTools\core\fieldMetadataV3;
use dsTools\core\dataFormatV2;
use dsTools\core\dataValidationV2;
use dsTools\dsSwissKnife;

include_once ("dsTools/core/tableProxyV2.php");

class table_facturation_factures extends tableProxyV2 {
	function __construct() {
		parent::__construct ();

		$this->name = "facturation_factures";
		$this->primaryKey = "ID";
		
		$fm = fieldMetadataV3::newIDField();
		$this->addField ( $fm );

		$fm = fieldMetadataV3::newCheckbox("supprimee", "Supprimée");
		$fm->visible = false;
		$this->addField($fm);

		// HIDDEN FIELD not used in DB
		$fm = new fieldMetadataV3 ("type", "type", dataFormatV2\DF_VARCHAR, 20);
		$fm->dataFormat->nullable = false;
		$fm->defaultValue = "facture";
		$fm->dataPersistence = fieldMetadataV3::DATAPERSISTENCE_NOPERSISTENCE;
		$fm->visible = false;
		$this->addField ( $fm );
		
		$fm = new fieldMetadataV3("noFacture", "# facture", dataFormatV2\DF_VARCHAR, 20);
        $fm->validators[0][] = new dsTools\core\dataValidationV2\validateCommon("!empty");
        $this->addField($fm);

        $fm = fieldMetadataV3::newDatebox("creationHorodatage", "Date", date("Y-m-d"));
		$this->addField($fm);

        $fm = new fieldMetadataV3("noClient", dsSwissKnife::lang("# client", "customer #"), dataFormatV2\DF_VARCHAR, 50 );
        $fm->validators[0][] = new dataValidationV2\validateCommon("!empty");
        $this->addField($fm);

		$fm = new fieldMetadataV3("nomClient", dsSwissKnife::lang("Nom", "Name"), dataFormatV2\DF_VARCHAR, 50);
        $fm->validators[0][] = new dataValidationV2\validateCommon("!empty");
		$this->addField($fm);

		$fm = new fieldMetadataV3("adresseClient", dsSwissKnife::lang("Adresse", "Address"), dataFormatV2\DF_VARCHAR, 100);
		$this->addField($fm);

		$fm = new fieldMetadataV3("adresse2Client", "", dataFormatV2\DF_VARCHAR, 100);
		$this->addField($fm);

		$fm = new fieldMetadataV3("villeClient", dsSwissKnife::lang("Ville", "City"), dataFormatV2\DF_VARCHAR, 100);
		$this->addField($fm);

		$fm = new fieldMetadataV3("provinceClient", dsSwissKnife::lang("Province", "Province"), dataFormatV2\DF_VARCHAR, 20);
		$fm->defaultValue = "Québec";
		$this->addField($fm);

		$fm = new fieldMetadataV3("paysClient", dsSwissKnife::lang("Pays", "Country"), dataFormatV2\DF_VARCHAR, 30);
		$fm->defaultValue = "Canada";
		$this->addField($fm);

		$fm = new fieldMetadataV3("codePostalClient", dsSwissKnife::lang("Code postal", "Postal code"), dataFormatV2\DF_VARCHAR, 10);
		$this->addField($fm);

		$fm = new fieldMetadataV3("telephoneMaisonClient", dsSwissKnife::lang("Tel. Maison", "Home phone"), dataFormatV2\DF_VARCHAR, 20);
		$this->addField($fm);
		
		$fm = new fieldMetadataV3("telephoneBureauClient", dsSwissKnife::lang("Tel. Bureau", "Home phone"), dataFormatV2\DF_VARCHAR, 20);
		$this->addField($fm);
		
		$fm = new fieldMetadataV3("telephoneFaxClient", dsSwissKnife::lang("Fax", "Fax"), dataFormatV2\DF_VARCHAR, 20);
		$this->addField($fm);
		
		$fm = new fieldMetadataV3("telephoneCellulaireClient", dsSwissKnife::lang("Cell.", "Cell."), dataFormatV2\DF_VARCHAR, 20);
		$this->addField($fm);
		
		$fm = new fieldMetadataV3("courrielClient", dsSwissKnife::lang("Courriel", "e-mail"), dataFormatV2\DF_VARCHAR, 100);
		$this->addField($fm);

		// Pied de facture
		$fm = new fieldMetadataV3("sousTotal", dsSwissKnife::lang("Sous-total", "Subtotal"), dataFormatV2\DF_MONEY, '', "textbox" );
		$fm->readOnly = true;
		$fm->styleHint = "money";
		$this->addField($fm);

		$fm = new fieldMetadataV3("TPS", dsSwissKnife::lang("TPS", "PST"), dataFormatV2\DF_MONEY, '', "textbox" );
		$fm->readOnly = true;
		$this->addField($fm);
		
		$fm = new fieldMetadataV3("TVQ", dsSwissKnife::lang("TVQ", "QST"), dataFormatV2\DF_MONEY, '', "textbox" );
		$fm->readOnly = true;
		$this->addField($fm);
		
		$fm = new fieldMetadataV3("total", dsSwissKnife::lang("Total", "Total"), dataFormatV2\DF_MONEY, '', "textbox" );
		$fm->readOnly = true;
		$this->addField($fm);
		
        $fm = new fieldMetadataV3("depot", dsSwissKnife::lang("Accompte", "Deposit"), dataFormatV2\DF_MONEY, '', "textbox" );
		$this->addField($fm);

		$fm = new fieldMetadataV3("depotPourcent", dsSwissKnife::lang("Accompte %", "Deposit %"), dataFormatV2\DF_PERCENT, 5.3, "textbox" );
		$fm->dataFormat->nullable = true;
		$this->addField($fm);

		$fm = new fieldMetadataV3("depotArgentOuPourcent", dsSwissKnife::lang("$ / %", "$ / %"), dataFormatV2\DF_BOOL, null, "checkbox" );
		$fm->visible = false;
		$this->addField($fm);

		$fm = new fieldMetadataV3("solde", "Solde", dataFormatV2\DF_MONEY, "", "textbox" );
		$fm->readOnly = true;
		$this->addField($fm);

		hooker::run(hooker::AFTER, "facturation_table_factures_construct", $this);
	}

}


