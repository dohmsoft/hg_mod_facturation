<?php


use dsTools\communicator;
use dsTools\dsPDOswissKnife;
use dsTools\SQLsafeQuery;
use dsTools\SQLsafeQueryWHEREclauseValue;
use dsTools\dsControlsRow;
use dsTools\core\dataValidationV2;
use dsTools\core\dataSource;
use dsTools\core\dataFormatV2;
use dsTools\core\fieldMetadataV3;

include_once("dsTools/dsControls.php");

modulesManager::loadModules (	"liste",
						                                "popup",
						                                "popupSettings",
						                                "facturationProduitsSelecteurPopup",
						                                "courriel",
						                                "textareaAutosize"
						                            );

/*
 * Défini un mode (soumission, facturation, contrat, autre.)
 */
class facturationTableset{
	public $name = "modeX";
	public $UIName = "{insert facturationMode name here}";
	//public $tpFacture = null;
	//public $tpLignes = null;

	function __construct($name, $UIName, &$tpFacture, &$tpLignes){
		$this->name = $name;
		$this->UIName = $UIName;
		$this->tpFacture = &$tpFacture;
		$this->tpLignes = &$tpLignes;
	}
}

class module_facturation extends base_module {
	// Do some work, offer some services

	public $facture = null; // @@todo remplacer les références vers tpFacture
	public $lignes = null;  // @@todo remplacer les  références vers tpLignes
	public $nbLignesMinAAfficher = 10;
	public $nbLignesVidesMin = 6;
    public $splitMultilineDescriptionThisLength = 0; // No split by default
    public $maxLinesPerPrintedPage = 19;
    public $tpFacture = null;  // active TP, either facture or soumission
    public $tpLignes = null;
    public $lst_lignes;

	public $tableSets = array();
	public $activeTableSetName;


    private $modeSoumissionOuFacturation;  //@deprecated remplacé par activeTableSet;

    function __construct($instanceName, $parent) {
        parent::__construct($instanceName, $parent);
		$this->UIName = "Soumission et facturation";

	    settinger::addNewGlobal($this, "saveInSession", null);

    // modes / tableSets
	    $tpFF = new table_facturation_factures(); $tpFL = new table_facturation_factures_lignes();
	    $ts = new facturationTableset("facture", "facture", $tpFF, $tpFL);
		$this->tableSets['facture'] = &$ts;
		$tpSF = new table_facturation_soumissions(); $tpSL = new table_facturation_soumissions_lignes();
		$tsSoumission = new facturationTableset("soumission", "soumission", $tpSF, $tpSL);
		$this->tableSets['soumission'] = &$tsSoumission;
		hooker::run( "AFTER", "module_facturation.tablesets", $this);

    // Settings
		// Settings are stored by default in the application storage
	    settinger::addNewGlobal($this, "settingsStoragesNames", array("application"));
		$settingsStoragesNames = settinger::getGlobal($this, "settingsStoragesNames");

	// Utilise la feature accompte% - accompte $ - solde
		$this->featureAccompte = true;

	// UI SETTINGS
	    $fm = new fieldMetadataV3("businessName", "Raison sociale", dsTools\core\dataFormatV2\DF_VARCHAR, 200);
	    $fm->defaultValue = "Slapp Technologies Inc.";
	    settinger::addNewGlobalWithUI($this, $fm, $settingsStoragesNames);

	    $fm = new fieldMetadataV3("depositAutoPercent", "% dépôt", dataFormatV2\DF_DECIMAL, 6.3);
	    $fm->defaultValue = "14.975";
	    settinger::addNewGlobalWithUI($this, $fm, $settingsStoragesNames);

	    $fm = new fieldMetadataV3("codeProduitExclus", "Code de produit pour exclusions", dataFormatV2\DF_TEXT, 20);
	    $fm->defaultValue = "XCLUS";
	    settinger::addNewGlobalWithUI($this, $fm, $settingsStoragesNames);

        linker::addJS("https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.min.js");

    // Set the mode before applying fieldsmetadata to the list module, because otherwise, tpLignes is not set
        $GET = new dsTools\core\dataValidation\validationWrapper($_GET);
        $GET->addValidation("type", new dataValidationV2\validateFromDataSource(new dsTools\core\dataSource\clsDataSourceSimpleArray(array("soumission", "facture"))));
        $this->setActiveTableset( $GET->get("type", "soumission"));

    // Liste
        $this->lst_lignes = new module_liste("lst_lignes", $this);
        $this->lst_lignes->fieldsMetadata = &$this->tpLignes->fieldsMetadata; // & to stick with whatever tp we'll set
		unset($this->lst_lignes->fieldsMetadata['idFacture']);

        $this->prodSelectorPopup = new module_popup("prodSelectorPopup", $this);
        $this->myPopup = new module_popup("myPopup", $this);

        $this->useMultilineDescription = true;
        $this->splitMultilineDescriptionThisLength = 70; // split long lines to new lines

    // load pageLeave
	    if (modulesManager::loadOptionalModule("pageLeave")){
		    $this->modPageLeave = new module_pageLeave("modPageLeave", $this);
	    }

    // config popup
	    $this->modPopupSettings = new module_popupSettings("modPopupSettings", $this);

    // load customerAutoFill
	    if (modulesManager::loadOptionalModule("facturation_customerAutoFill")){
		    $this->modFacturationCustomerAutoFill = new module_facturation_customerAutoFill("modFacturationCustomerAutoFill", $this);
	    }

    // courriels
	    $this->mod_courriel = new module_courriel("mod_courriel", $this);

	    // load email tracker
	    if (modulesManager::loadOptionalModule("courrielTracker")){
		    $this->modCourrielTracker = new module_courrielTracker("modCourrielTracker", $this);
	    }
    }

    /*
     * @deprecated
     * only for backward compatibility
     */
    public function setModeSoumissionOrFacture($isSoumissionOrFacture){
        $this->modeSoumissionOuFacturation = $isSoumissionOrFacture;
        if ($isSoumissionOrFacture){
        	$this->setActiveTableset("soumission");
        }
        else{
			$this->setActiveTableset("facture");
        }
    }
    public function getModeSoumissionOrFacture(){
    	return ($this->activeTableSetName == "soumission");
    }
    public function getModeSoumissionOrFactureString(){
        return $this->activeTableSetName;
    }


    public function setActiveTableset($tablesetName){
	    $ts = $this->tableSets[$tablesetName];

    	$this->activeTableSetName = $ts->name;
	    $this->tpFacture = $ts->tpFacture;
	    $this->tpLignes = $ts->tpLignes;
    }


	function load($idFacture = null){
        if ($idFacture == null) {
            // Essaie de loader une facture mise de côté dans la session
            $data = settinger::getGlobal($this, "saveInSession");
            if ($data != null){
                $this->tpFacture->data = $data['facture'];
                $this->tpLignes->data = $data['lignes'];
	            $this->fillMinRows();

                settinger::setGlobal($this, "saveInSession", null);
                return;
            }
            else{
                return $this->loadBlank();
            }
        }

        $this->tpFacture->loadRowByID($idFacture);
        $this->tpFacture->loadForeignKeyChildRows($this->tpLignes);

        $this->fillMinRows();
	}

    function loadBlank(){
        $this->tpFacture->data = $this->tpFacture->getDefaultData();

        // Set the would-be noFacture
        $this->tpFacture->data['noFacture'] = $this->getProbableNextNoFacture();

        // Set the pourcentage
        $this->tpFacture->data['depotPourcent'] = settinger::getGlobal($this, 'depositAutoPercent');

        $this->fillMinRows();
    }

	/* New way of generating PDF
	 * Need to supply &type= and &id in the querystring for load to work
	 * Also, the page needs to return null so default.php can output raw content
	 */
	function loadAndStreamPDFToBrowser($idFacture){
		$this->load($idFacture);
		$outputfilename = $this->overridableSelectTemplateAndReturnPDFfilename();

		$path = $outputfilename;
		header("Content-Length: " . filesize ( $path ) );
		header("Content-type: application/pdf");  // octet-stream pour ouvrir dans un autre onglet mais marchera pas avec les iShits
		header("Content-disposition: inline; filename=".basename($path));
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

		readfile($path);
	}

    /* Une  facture vide est créé puis les champs fournis écrasent */
    function createWithGivenData($factureDataArray = array(), $lignesDataArray = array()){
        $this->tpFacture->data = $this->tpFacture->getDefaultData();
        $this->tpFacture->data['noFacture'] = $this->getProbableNextNoFacture(); // Set the would-be noFacture
        $this->tpFacture->data = array_merge($this->tpFacture->data, $factureDataArray);

        // lignes
        $emptyRow = $this->tpLignes->getEmptyData();
        foreach ($lignesDataArray as $row){
            $row = array_merge($emptyRow, $row);
            $this->tpLignes->data[] = $row;
        }

        $this->fillMinRows();
    }

    /* s'assure d'avoir au moins $this->nbLignesMinAAfficher nombre de lignes */
    /* @@ use the module_liste's new feature */
    private function fillMinRows(){
        $nbLignes = count($this->tpLignes->data);
        $blankLinesToAdd = max($this->nbLignesMinAAfficher - $nbLignes, 0); // si moins de ligne que le min à afficher
        $blankLinesToAdd = max($this->nbLignesVidesMin, $blankLinesToAdd); // au moins X lignes vides.

        if ($blankLinesToAdd > 0 ){
            // Make an empty row
            $emptyRow = $this->tpLignes->getEmptyData();

            for ( $i = 0; $i < $blankLinesToAdd; $i++ ){
                $this->tpLignes->data[] = $emptyRow;
            }
        }
    }

    /* save temporairement dans la session
        et reload automatique
        utile pour créer une facture à partir d'un autre module
        et redirecter vers la page facture
    */
    function saveInSession(){
        settinger::setGlobal($this, "saveInSession", array("facture"=>$this->tpFacture->data, "lignes"=>$this->tpLignes->data));
    }


    // @@TODO: upgrade to tableProxy's save
    function save(){

        $idFacture = $this->tpFacture->data['ID'];
	    $newInvoice = (empty($idFacture));

        $tableName = $this->tpFacture->name;

    // # facture doublon ?
        $ssq = new \dsTools\dsSQLquery();
        $ssq->FROM($tableName);
        $ssq->SELECT("ID");
        $ssq->WHERE(new \dsTools\dsSQLquery\clauses\WHEREvalueShort("noFacture", $this->tpFacture->data['noFacture']));
        $ssq->WHERE(new \dsTools\dsSQLquery\clauses\WHEREvalueShort("supprimee", 0));
        $list = $ssq->execute();
        if ( (bool)$list && $list[0]['ID'] != $idFacture ){
            $this->myPopup->showMessage("Le document '" . $this->tpFacture->data['noFacture'] . "' existe déjà.");
            return false;
        }

    // SAVE
        $PDOsk = new dsTools\dsPDOswissKnife();
        $retLineID = null;
        $dataWithoutID = $this->tpFacture->data;
        unset($dataWithoutID['ID']);
        $ret = $PDOsk->writeEz($tableName, $idFacture, $dataWithoutID, $retLineID);
        if ( $ret == false ){
            $this->myPopup->showMessage("Erreur de sauvegarde.");
            return false;
        }

        // si on vient de créer la facture
        if ($idFacture == null) $idFacture = $retLineID;

        // Trouve la dernière ligne avec du contenu
        // Ordre inverse pour trimmer les lignes vides

        $lastLineNb = count($this->tpLignes->data) -1;
        $emptyLinesStartIx = $lastLineNb + 1;

        for ( $i = $lastLineNb; $i > 0; $i-- ) {
            $row = $this->tpLignes->data[$i];

            // Skip empty rows at the bottom
            if (empty($row['ID']) && $row['code'] == "" && $row['description'] == "" && $row['qtee'] == "" && $row['prixUnitaire'] == "") {
                $emptyLinesStartIx = $i;
            }
            else{
                break;
            }
        }

        // Force le # de facture
        // et sauvegarde les lignes

        foreach ($this->tpLignes->data as $ix=>$row){
            if ( $ix == $emptyLinesStartIx ) break;  // rendu où y'a pu de contenu

            $row['idFacture'] = $idFacture;
            $ID = ($newInvoice == true) ? null : $row['ID'];
            unset($row['ID']);
            $ret = $PDOsk->writeEz($tableName."_lignes", $ID, $row);
            if ($ret == false) break;
        }

        if ($newInvoice){
            $this->tpFacture->data['ID'] = $idFacture;
            $elAddr = $this->getMyAddressAsString() . ".ID";
            communicator::setFieldValue($elAddr, $idFacture);
        }

        if ($ret !== false) {
	        if (isset($this->modPageLeave)) $this->modPageLeave->clean();
	        return true;
        }
    }


    function delete($idFacture){
        $tableName = $this->tpFacture->name;
        dsPDOswissKnife::updateEz($tableName, $idFacture, array("supprimee"=>1));
    }

    // Retourne le prochain numéro de facture
    // Il est possible que quelqu'un d'autre sauvegarde ce # avant nous
    public $firstNoFacture = 0;

    function getProbableNextNoFacture(){
		$sql = new \dsTools\dsSQLquery();
		$sql->limit = 1;
        $sql->SELECT("CAST(noFacture AS DECIMAL(20,0)) as no");
        $sql->FROM($this->tpFacture->name);
        $sql->ORDERBY("CAST(noFacture AS DECIMAL(20,0)) DESC");
        $list = $sql->execute();

        $lastNo = ( (bool)$list ) ? $list[0]["no"] : $this->firstNoFacture;

        return intval($lastNo) + 1;
    }


    protected function render(){
		hooker::runV2($this, "render");
		$this->jsProps("featureAccompte");

        $data = $this->tpFacture->data;
        $dataMetadataHelper = new dsTools\core\dataMetadataHelper();
        $parentObjectsArray = array($this);
        $cb  = new \dsTools\dsControlsV3\controlsBuilder($this->tpFacture, -1, $this);
        $l = function($fieldName) use ($cb) { return $cb->buildLabelOnly($fieldName);};
		$f = function($fieldName) use ($cb) { return $cb->buildRaw($fieldName);};
		$r = function($fieldName) use ($cb) { return $cb->buildComplete($fieldName);};
        //$dsControlsRow = new dsTools\dsControlsRow($this->tpFacture, $data, $dataMetadataHelper, $parentObjectsArray);

        //$l = function($fieldName) use ($dsControlsRow) { return $dsControlsRow->renderToString($fieldName, true, false);};
        //$f = function($fieldName) use ($dsControlsRow) { return $dsControlsRow->renderToString($fieldName, false, true);};
        //$r = function($fieldName) use ($dsControlsRow) { return $dsControlsRow->renderToString($fieldName);};

        // lignes
        $this->lst_lignes->load($this->tpLignes->data);

        ?>

	    <i class="fa fa-gear fa-2x pull-left" style="cursor:pointer;" onclick="ajaxEventV2(this, 'settings')"></i>
        <table id="facture">
            <tr id="factureEntete">
                <td>
                    <table id="infoFacture">
                        <tr>
                            <td class="leftHeader">
                                &nbsp;<?php  hooker::runV2($this, "renderLeftHeader"); ?>
                            </td>
                            <td class="titre">
                                <?=strtoupper($this->getModeSoumissionOrFactureString())?>
                            </td>
                            <td class="rightHeader">
                                <?=$r("ID")?>
                                <?=$l("noClient")?><?=$f("noClient")?>
                                <div class="fieldLabel noFacture"># <?=$this->getModeSoumissionOrFactureString()?></div><?=$f("noFacture")?>
                                <?=$l("creationHorodatage")?><?=$f("creationHorodatage")?>
	                            <?php  hooker::runV2($this, "renderRightHeader", hooker::AFTER); ?>
                            </td>
                        </tr>

                    </table>

                    <table id="client">
                        <thead>
                        <tr>
                            <th colspan="2">CLIENT</th>
                        </tr>
                        </thead>
                        <tr>
                            <td>
                                <?=$r("nomClient")?>
                                <?=$r("adresseClient")?>
                                <?=$r("adresse2Client")?>
                                <?=$r("villeClient")?>
                                <div style="display:none;"><?=$f("provinceClient")?></div>
                                <?=$r("codePostalClient")?>
                            </td>

                            <td>
                                <?=$r("telephoneMaisonClient")?>
                                <?=$r("telephoneBureauClient")?>
                                <?=$r("telephoneCellulaireClient")?>
                                <?=$r("courrielClient")?>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr id="lignes">
                <td>
                    <?php echo $this->lst_lignes->renderToString();?>
                </td>
            </tr>
            <tr id="pied">
                <td class="totaux">
                    <?=$r("sousTotal")?>
                    <?=$r("TPS")?>
                    <?=$r("TVQ")?>
                    <?=$r("total")?>
                    <div style="<?= ($this->featureAccompte) ? "" : "display:none;" ?>">
	                    <?=$r("depotPourcent")?>
	                    <?=$r("depot")?><?=$r("depotArgentOuPourcent")?>
	                    <?=$r("solde")?>
                    </div>
                </td>
            </tr>
            <tr id="boutons">

                <td>
                    <?php
                    hooker::runV2($this, "renderButtons", hooker::BEFORE);
                    ?>
                    <button name="btnSave" onclick="ajaxEventV2(this, 'sauvegarder')"><img src="projet/icones/save.png">Sauvegarder</button>
                    <!--button onclick="ajaxEventV2(this, 'tcPDF', '', {showWaitNow: true})"><img src="projet/icones/pdf.png">Version imprimable</button-->
                    <a name ="btnPDF" class="buttonStyle buttonLarge" href="<?=$_SERVER['REQUEST_URI']?>&pdf"><img src="projet/icones/pdf.png">Version imprimable</a>
	                <button name="btnEmail" onclick="ajaxEventV2(this, 'courriel', '', {showWaitNow: true})"><img src="projet/icones/email.png">Envoyer par courriel</button>
                    <?php if ( $this->getModeSoumissionOrFacture() == true ){ ?>
                        <button name="btnFacture" onclick="ajaxEventV2(this, 'transformer')"><img src="projet/icones/contrat.png">Transformer en contrat</button>
                    <?php }?>

                </td>

            </tr>
            <?php
                hooker::runV2($this, "renderButtons", hooker::AFTER);
            ?>
        </table>

	    <?php
	    hooker::runV2($this, "render", hooker::AFTER);

    }


	function event_settings($eventInfo){
		$this->modPopupSettings->showSettings();
	}

    protected function loadFromBrowserData(&$eventInfo, $saveIfValid = true){
		$wdpFacture = new \dsTools\core\webDataParser($eventInfo['decodedFormsData']);
		$wdpLignes = new \dsTools\core\webDataParser($eventInfo['decodedFormsData']);

		$wdpFacture->parse($this, $this->tpFacture);
		$wdpLignes->parseMultipleRows($this->lst_lignes, $this->tpLignes);

		$ret = ($wdpFacture->dataIsValid && $wdpLignes->dataIsValid);

		if ($ret == true){
			if ($saveIfValid) $this->save();
		}else{
			$this->myPopup->showMessage("Erreur dans le formulaire, veuillez les corriger avant de réessayer");
		}

		return array($ret, &$wdpFacture, &$wdpLignes);
    }


    private function prepDataForMultipagesOutput(){
        // Split long lines to new lines
        $tempRows = array();
        if ($this->splitMultilineDescriptionThisLength > 0){
            foreach ($this->tpLignes->data as $row){
                $splits = array();

                // Split at new line
                $descNewlineSplits = explode("\n", $row['description']);

                // Re-split in large chunks
                foreach ( $descNewlineSplits as $descNewlineSplit){
                    $lengthwiseSplits = wordwrap($descNewlineSplit, $this->splitMultilineDescriptionThisLength, "<<SPLIT>>", true);
                    $lengthwiseSplits = explode("<<SPLIT>>", $lengthwiseSplits);
                    $splits = array_merge($splits, $lengthwiseSplits);
                }

                // Build virtual new lines
                $isFirstSubLine = true;
                foreach ($splits as $split){
                    $zRow = $row;
                    $zRow['description'] = $split;
                    if ( $isFirstSubLine ) {
                        $isFirstSubLine = false;
                    }
                    else{
                        $zRow['code'] = "";
                        $zRow['qtee'] = "";
                        $zRow['prixUnitaire'] = "";
                        $zRow['prixExt'] = "";
                    }
                    $tempRows[] = $zRow;
                }
            }
        }
        else{
            $tempRows[] = $this->tpLignes->data;
        }

        // Trim trailing empty lines
        for ($i = count($tempRows)-1; $i >= 0; $i--){
            if ( $tempRows[$i]['description'] == "" ) {
                unset($tempRows[$i]);
            }
            else{
                break;
            }
        }

        return array_values($tempRows); // Reindex
    }

    // @return the pdf temp file
    function toPDF($templateFileNameFullPath, $prependTemplatesArray = array(), $appendTemplatesArray = array(), $outputFilename = null){
        modulesManager::loadModules("tcpdf");
        $tc = new TCPDF('P', 'in', 'LETTER');
        //$tc = new Com\Tecnick\Pdf\Tcpdf("in");

        $tc->setFontSubsetting(false);

        //$tc->setMargins(0.25,0.25,0.25,true);
        $tc->setMargins(0,0,0,true);

        $tc->setPrintHeader(false);
        $tc->setPrintFooter(false);
        $tc->SetAutoPageBreak(TRUE, 0);

        $pages = array();

        // Pages à ajouter AVANT les pages de facturation
        foreach($prependTemplatesArray as $fileName){
            $pages = array_merge($pages, $this->fillTemplate($fileName));
        }

        // Fill the template
        $pages = array_merge($pages, $this->fillTemplate($templateFileNameFullPath));

        // Pages à ajouter APRES les pages de facturation
        foreach($appendTemplatesArray as $fileName){
            $pages = array_merge($pages, $this->fillTemplate($fileName));
        }

        // search&replace page count
        $pageCount = count($pages);
        $curPageIx = 1;
        foreach($pages as &$page){
            $page = str_replace(array("%page%", "%pages%"), array($curPageIx, $pageCount), $page);
            $curPageIx ++;
        }


        // SORTIE
        foreach($pages as $pge){
            $tc->AddPage();
            $tc->ImageSVG("@".$pge, "0","0", 8.5, 11,"","","");
        }

        if ($outputFilename == null) $outputFilename = \dsTools\dsSwissKnife::getNewTempFilename("", ".pdf");
        $tc->Output($outputFilename, "F");
        return $outputFilename;
    }

    function overridableSelectTemplateAndReturnPDFfilename(){
        // choose the template
        $template = "projet/documents/soumission.svg";
        if ($this->activeTableSetName == "facture") $template = "projet/documents/facture.svg";

        // svg to pdf
        $outputfilename = $this->toPDF($template, array());
        return $outputfilename;
    }

    function fillTemplate($filenameFullPath){
        $pages = array();
        $html = file_get_contents($filenameFullPath);

        $nbOfRowsInTemplate = substr_count($html, "%description");

        // search & replace
        $search = array();
        $replace = array();

    // HEADER / FOOTER FUNC
        $headerFooterSearchReplace = function(&$template, $isWithFooter){
            static $footerFields = array("sousTotal", "TPS", "TVQ", "total", "depot", "solde");

            foreach ($this->tpFacture->fieldsMetadata as $fieldName => $fm){
                $val = (isset($this->tpFacture->data[$fm->name])) ? $this->tpFacture->fieldsMetadata[$fieldName]->dataFormat->toPretty($this->tpFacture->data[$fm->name]) : "";
                if ($isWithFooter == false && in_array($fieldName, $footerFields)) $val = ""; // le footer n'apparaît que sur la dernière page
                $searchCode = $fm->name;
                $this->fillTemplateFieldMingle($fm, $val, $searchCode);
                $search[] = "/% *" . $searchCode . " *%/i";
                $replace[] = $this->svgEntities($val);
            }

            $template = preg_replace($search, $replace, $template);
        };

    // PREP LINES
        $splittedRows = $this->prepDataForMultipagesOutput();

        // Add empty rows to fill the last page
        if ($nbOfRowsInTemplate > 0){
            $rowsToAdd = $nbOfRowsInTemplate - (count($splittedRows) % $nbOfRowsInTemplate);
            for ($i = 0; $i < $rowsToAdd; $i++){
                $splittedRows[] = $this->tpLignes->getEmptyData();
            }
        }

        $search = array();
        $replace = array();
        $curTemplateRowID = 0;

        $lignesFieldsNames = array_keys($this->tpLignes->fieldsMetadata);

        $dataRowCount = count($splittedRows);

        for ($i = 0; $i < $dataRowCount; $i++){
            foreach ($lignesFieldsNames as $fieldName){
                $searchCode = $fieldName;
                $val = $this->tpLignes->fieldsMetadata[$fieldName]->dataFormat->toPretty($splittedRows[$i][$fieldName]);
                $this->fillTemplateFieldMingle($this->tpLignes->fieldsMetadata[$fieldName], $val, $searchCode);
                $search[] = "/% *" . $searchCode . $curTemplateRowID. " *%/i";
                $replace[] = $val;
            }
            $curTemplateRowID++;

            // New page ?
            $isLastPage = (($i + 1) == $dataRowCount);
            if ($curTemplateRowID == $nbOfRowsInTemplate || $isLastPage ){
                $curPage = preg_replace($search, $replace, $html);
                $headerFooterSearchReplace($curPage, $isLastPage);
                $pages[] = $curPage;
                $search = array();
                $replace = array();
                $curTemplateRowID = 0;
            }
        }

        return $pages;

    }

    //
    private function fillTemplateFieldMingle($fm, &$value, &$retFieldSearchCode){
        $retFieldSearchCode = $fm->name;

        switch($fm->name){
            case "creationHorodatage":
                $retFieldSearchCode = "dateFacture";
                break;
            case "qtee":
                $retFieldSearchCode = "q";
                $value = str_pad($value, 6, " ", STR_PAD_BOTH);
                break;
            case "prixUnitaire":
                $retFieldSearchCode = "pu";
                $value = str_pad($value, 10, " ", STR_PAD_LEFT);
                break;
            case "prixExt":
                $retFieldSearchCode = "pe";
                $value = str_pad($value, 10, " ", STR_PAD_LEFT);
                break;
            case "sousTotal":
            case "TPS":
            case "TVQ":
            case "total":
            case "depot":
            case "solde":
                $value = str_pad($value, 11, " ", STR_PAD_LEFT);
                break;


        }

    }

    function svgEntities($text){
        $search = array(    '<',    '>',           '"',        '&');
        $replace = array(   '&lt;', '&gt;',   '&quot;',    '&amp;');

        $text = str_replace($search, $replace, $text);
        return $text;
    }

    function event_productCodeChanged($eventInfo){

        $this->prodSelectorPopup->blackBG = false;
        $this->prodSelectorPopup->modal = false;
        $this->prodSelectorPopup->positionRelativeTo = "page";
        $this->prodSelectorPopup->positionX = $eventInfo['payload']['posLeft'];
        $this->prodSelectorPopup->positionY = $eventInfo['payload']['posTop'];

        $container = new module_facturationProduitsSelecteurPopup("prodSelect", $this);
        $container->load($eventInfo['payload']['prodCode']);
        $this->prodSelectorPopup->showContainer($container);

    }


    function event_sauvegarder($eventInfo){

        // Sauvegarder si valide
	    list($ret, $wdpFacture, $wdpLignes) = $this->loadFromBrowserData($eventInfo, true);

	    if ($ret == false) return;

        $type = $this->activeTableSetName;

        $newURL = "default.php?page=facture&type=$type&id=" . $this->tpFacture->data['ID'];

        $evt = new \dsTools\core\events\jsEvent("onclick", "window.location.href='" . $newURL . "'");
        $this->myPopup->showMessage("Sauvegarde réussie!", $evt);
        return true;
    }


    function event_tcPDF($eventInfo){
        $factureDRB = null;
        $lignesDRsB = null;

	    // Sauvegarder si valide
	    list($ret, $wdpFacture, $wdpLignes) = $this->loadFromBrowserData($eventInfo, true);
	    if ($ret == false) return;

        $outputfilename = $this->overridableSelectTemplateAndReturnPDFfilename();
        dsTools\communicator::addMessage("downloadFile", array("URL"=>"temp/" . basename($outputfilename)));
        //dsTools\communicator::addMessage("redirect", "temp/" . basename($outputfilename));

    }

    function event_courriel($eventInfo){
        if (isset($GLOBALS['myConfig']['demoMode']) && $GLOBALS['myConfig']['demoMode'] == "demo"){
            $this->myPopup->showMessage("L'envoi par courriel est désactivé en mode démonstration. <br>Un courriel est envoyé à l'adresse du client avec un PDF en pièce jointe. Une copie est également envoyée à l'adjointe");
            return;
        }

	    // Sauvegarder si valide
	    list($ret, $wdpFacture, $wdpLignes) = $this->loadFromBrowserData($eventInfo, true);
	    if ($ret == false) return;

	    // A-t-on une adresse de destination ?
	    if (empty($this->tpFacture->data['courrielClient'])){
		    $this->myPopup->showMessage("Veuillez d'abord entrer l'adresse courriel du client.");
		    return;
	    }

	    // PDF


        // svg to pdf
        $outputfilename = $this->overridableSelectTemplateAndReturnPDFfilename();

	    $facSoum = $this->activeTableSetName;
        $facSoumPretty = ucfirst($facSoum) . " #" . $this->tpFacture->data['noFacture'];

        $this->mod_courriel->addAttachment($outputfilename, $facSoum . $this->tpFacture->data['noFacture'] . ".pdf");

        $businessName = settinger::getGlobal($this, "businessName");

		// set the content for my customer //
	    ob_start();
	    $this->overridableRenderEmailContents();
	    $content = ob_get_clean();

        $ret = $this->mod_courriel->send($facSoumPretty . " - " . $businessName,
				                            $content,
				                            $this->tpFacture->data['courrielClient']);

        //// ne pas envoyer au CC si pas envoyé au client
        //if (!$ret) return;

        // now send it to my CC
	    ob_start();
	    $this->overridableRenderEmailContents(true);
	    $content = ob_get_clean();

	    $this->mod_courriel->send($facSoumPretty . " - " . $businessName,
								    $content,
								    settinger::getGlobal($this->mod_courriel, "BCC"));

    }

    /*
     * override this and set the content yourself
     * the caller will catch the output buffer to ease your work
     *
     * @isCCversion: désactive le tracking courriel quand on se l'envoi ou au comptable, etc.
     */
    function overridableRenderEmailContents($isCCversion = false){
		$myURL = "";
	    if (isset($this->modCourrielTracker) && !$isCCversion) {
			$myURL = $this->modCourrielTracker->generateTrackingURL($this->tpFacture->name, $this->tpFacture->data['ID']);
		}

		    ?>
            <html>
                <body>
	                <p>Ci-joint votre <?=$this->getModeSoumissionOrFactureString()?></p>
					<p>Veuillez nous confirmer la réception de ce courriel en y répondant
						<?php if ($myURL != ""){ ?>
							ou <a href="<?=$myURL?>&type=human">en cliquant sur ce lien</a>
						<?php }?>
	                </p>
	                <img src="<?=$myURL?>">
                </body>
            </html>
            <?php
    }

    function event_transformer($eventInfo){

    	// Sauvegarder si valide
	    list($ret, $wdpFacture, $wdpLignes) = $this->loadFromBrowserData($eventInfo, true);

	    if ( $ret == true )
	    {
		    // Switch en mode facture
		    $fact = $this->tpFacture->data;
		    $lignes = $this->tpLignes->data;

		    $this->setActiveTableset("facture");

		    $this->tpFacture->data = $fact;
		    $this->tpLignes->data = $lignes;
		    $this->tpLignes->isDataMultiRows = true;

		    $this->tpFacture->data['ID'] = null;
		    $this->tpFacture->data['noFacture'] = $this->getProbableNextNoFacture();

		    $this->saveInSession();
		    $this->reloadPageWithBlankInvoiceAfterTransform();
	    }
	    else{
		    return;
	    }

    }

    protected function reloadPageWithBlankInvoiceAfterTransform(){
        $cur = $this;
        do{
            if ($cur instanceof base_page) break;
            if ($this->parent === null) return;
            $cur = $this->parent;
        }while(1);

        $name = substr(get_class($cur), 5);
        $type = $this->activeTableSetName;
        communicator::addMessage("redirect", '?page=' . $name . '&type=' . $type);

    }


}


