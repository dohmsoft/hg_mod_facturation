
var module_facturation = new Container("module_facturation",{
	init: function(){
		this.curRowEditIx = "";

	},


	documentReady:function(){
		if ($("." + this.moduleName).length > 0) {
			// Téléphones
			$(this.field("telephoneMaisonClient")).mask('(000) 000-0000');
			$(this.field("telephoneBureauClient")).mask('(000) 000-0000 x00000');
			$(this.field("telephoneCellulaireClient")).mask('(000) 000-0000');
			$(this.field("codePostalClient")).mask('S0S 0S0', {onKeyPress: function (value, event) {
				event.currentTarget.value = value.toUpperCase();
			}});

			// Calculer tu suite et quand y'aura des changements
			module_facturation.calculer();

		}

		// Si on clique sur un produit dans le popup
		$("body").on("click", "#popup_prodSelectorPopup tbody tr", function () {
			module_facturation.copyRowFromPopup(this);
			module_popup.close("prodSelectorPopup");
		});
	},



	calculer:function(el){
		var sousTotal = 0;
		var tauxTPS = 5;   // @@ HARDCODED TAXES :(
		var tauxTVQ = 9.975;

		var exclusionCode = "XCLUS"; //@@ TODO: implémenter les configs transférables vers js
		var sectionExclusion = false;

		$("#lignes table tbody tr").each(function(){
			var qtee = dsSwissKnife.unPrettyPrint("float", $(".qtee .input", this).val());
			var prixUnitaire = dsSwissKnife.unPrettyPrint("money", $(".prixUnitaire .input", this).val());
			var code = $(".code .input", this).val();
			var prixExt = "";

			if (code == exclusionCode) sectionExclusion = true;

			if ( !isNaN(qtee) && !isNaN(prixUnitaire) ) {
				prixExt = dsSwissKnife.round(qtee * prixUnitaire);

				// si on est dans la zone d'exclusion, ne pas additionner au sous-total //
				if ( sectionExclusion == false ) sousTotal += prixExt;
			}

			$(".prixExt .input", this).val(dsSwissKnife.prettyPrint("money",prixExt));
		});



		var TPS = dsSwissKnife.round(sousTotal * tauxTPS / 100);
		var TVQ = dsSwissKnife.round(sousTotal * tauxTVQ / 100);
		var total = dsSwissKnife.round(sousTotal + TPS + TVQ);

		$(".totaux [name=sousTotal].input").val(dsSwissKnife.prettyPrint("money",sousTotal));
		$(".totaux [name=TPS].input").val(dsSwissKnife.prettyPrint("money",TPS));
		$(".totaux [name=TVQ].input").val(dsSwissKnife.prettyPrint("money",TVQ));
		$(".totaux [name=total].input").val(dsSwissKnife.prettyPrint("money",total));

	// Calcul du dépôt //

		var isDepotArgent = $(".totaux [name=depotArgentOuPourcent]").prop('checked');
		var depot = dsSwissKnife.unPrettyPrint("money", $(".totaux [name=depot].input").val());
		var depotPourcent = module_facturation.fieldValue("depotPourcent").fromPercent();

		if ( isDepotArgent != 1 ){
			depot = sousTotal * depotPourcent.value / 100;
		}
		else{
			depotPourcent = new dsValueFormatter(depot / sousTotal * 100);
		}
		var solde = dsSwissKnife.round(total - depot);
		module_facturation.field("depotPourcent").val(depotPourcent.toPercent(3).value);

		$(".totaux [name=depot].input").val(dsSwissKnife.prettyPrint("money", depot));
		$(".totaux [name=solde].input").val(dsSwissKnife.prettyPrint("money", solde));
	// Calcul du dépôt //

	},

	copyRowFromPopup: function(popupRowEl){
	var prodCode = $(".code .input", popupRowEl).val();
	var description = $(".description .input", popupRowEl).val();
	var prixVendant = $(".prixVendant .input", popupRowEl).val();

	var Ix = module_facturation.curRowEditIx;
	$("#lignes [name='" + Ix + "'] .code .input").val(prodCode);
	$("#lignes [name='" + Ix + "'] .description .input").val(description);
	$("#lignes [name='" + Ix + "'] .prixUnitaire .input").val(prixVendant);
	var qteeInputField = $("#lignes [name='" + Ix + "'] .qtee .input");
	if ( prixVendant != 0 ) qteeInputField.val(1); // ne met pas de qtee pour les items sans prix
	qteeInputField.focus();
	qteeInputField.select();

	// refresh l'autosize de la description
	autosize.update($("#lignes [name='" + Ix + "'] .description .input"));
	},

	notmyevents: {
		'.lst_lignes.code': {   // commence par un . donc relatif à mon adresse
			keyup: function (event) {
				module_facturation.curRowEditIx = $(event.target).closest("tr").attr("name");

				// close if ESC
				if (event.keyCode == 27) {
					module_popup.close("prodSelectorPopup");
					return;
				}

				// fake first row click if enter
				if (event.keyCode == 13) {
					var firstRow = $("#popup_prodSelectorPopup tbody tr:eq(0)");
					if (firstRow.length > 0) {
						module_facturation.copyRowFromPopup(firstRow[0]);
						module_popup.close("prodSelectorPopup");
					}
					return;
				}

				// @@ timer pour pas envoyer trop d'�v�nements au serveur
				var elPos = $(event.target).offset();
				var payload = {
					posTop:  elPos.top,
					posLeft: event.target.clientWidth + elPos.left,
					prodCode:$(event.target).val()
				};
				ajaxEvent("", $("[name=lst_lignes]")[0], "productCodeChanged", payload );
			}
		},
		'.lst_lignes.description': {
			focusin: function(){
				module_popup.close("prodSelectorPopup");
			}
		},
		'.lst_lignes.qtee': {
			blur: function(){module_facturation.calculer()}
		},
		'.lst_lignes.prixUnitaire': {
			blur: function(){
				var prixUnitaire = dsSwissKnife.unPrettyPrint("money", $(event.target).val() );
				$(event.target).val(dsSwissKnife.prettyPrint("money", prixUnitaire));
				module_facturation.calculer();
			}
		}
	},

	events:{

		depotPourcent:{
			change: function(){
				this.field("depotArgentOuPourcent").prop('checked', false);
				module_facturation.calculer();
			}
		},
		depot:{
			change: function(){
				this.field("depotArgentOuPourcent").prop('checked', true);
				module_facturation.calculer();
			}
		}

}
});

