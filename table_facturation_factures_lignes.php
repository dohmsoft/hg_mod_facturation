<?php
use dsTools\core\tableProxyV2;
use dsTools\core\fieldMetadataV3;
use dsTools\core\dataFormatV2;
use dsTools\dsSwissKnife;


class table_facturation_factures_lignes extends tableProxyV2{
	function __construct(){
		parent::__construct();

		$this->name = "facturation_factures_lignes";
		$this->primaryKey = "ID";
		$this->addForeignKeyParentTable("idFacture", "facturation_factures", "ID");

		$fm = fieldMetadataV3::newIDField();
		$this->addField ( $fm );

		$fm = new fieldMetadataV3 ( "idFacture", "idFacture", dataFormatV2\DF_UINT32, "", "textbox" );
		$fm->defaultValue = "";
		$fm->readOnly = true;
		$fm->visible = false;
		$this->addField ( $fm );

        $fm = new fieldMetadataV3 ("code", "Code", dataFormatV2\DF_VARCHAR, 20);
        $this->addField($fm);

		$fm = new fieldMetadataV3 ( "description", "Description", dataFormatV2\DF_VARCHAR, 1024, "textarea" );
		$fm->customAttributes['data-autosize'] = "true";
		$fm->customAttributes['rows'] = "1";
		$this->addField ( $fm );

		$fm = new fieldMetadataV3 ( "qtee", dsSwissKnife::lang("Qté", "Qty"), dataFormatV2\DF_FLOAT, null, "textbox" );
        $fm->dataFormat->nullable = true;
		$fm->defaultValue = null;
        $fm->validators["empty"] = new dsTools\core\dataValidation\validateCommon("empty");
		$this->addField ( $fm );

		$fm = new fieldMetadataV3 ( "prixUnitaire", dsSwissKnife::lang("Prix", "Price"), dataFormatV2\DF_MONEY,"", "textbox" );
        $fm->validators["empty"] = new dsTools\core\dataValidation\validateCommon("empty");
		$fm->styleHint = "money";
		$this->addField ( $fm );

		$fm = new fieldMetadataV3 ( "prixExt", dsSwissKnife::lang("Prix ext.", "Ext. price"), dataFormatV2\DF_MONEY,"", "textbox" );
		$fm->readOnly = true;
		$fm->styleHint = "money";
		$this->addField ( $fm );
	}
}
