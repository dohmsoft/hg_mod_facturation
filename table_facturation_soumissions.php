<?php
use dsTools\core\tableProxyV2;
use dsTools\core\fieldMetadataV3;
use dsTools\core\dataFormatV2;
use dsTools\core\dataValidationV2;
use dsTools\dsSwissKnife;

include_once ("dsTools/core/tableMetadata.php");
modulesManager::includeAFileInMyModuleBaseDir("facturation", "table_facturation_factures.php");

class table_facturation_soumissions extends table_facturation_factures {
	function __construct() {
		parent::__construct ();

		$this->name = "facturation_soumissions";

		$this->fieldsMetadata['noFacture']->label = "# soumission";

		hooker::run(hooker::AFTER, "facturation_table_soumissions_construct", $this);
	}

}


